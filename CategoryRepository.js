const request = require('request');

/**
 * Created by vladyslavviotsekhovskyi on 03.08.2023.
 */

const CategoryRepositoryException = require('./Exceptions/CategoryRepositoryException');

class CategoryRepository {
    static getSalesforceBearerToken(jwtToken) {
        if (jwtToken === '') {
            throw new CategoryRepositoryException('JWT token is required');
        }
        return new Promise((resolve, reject) => {
            let result = {
                salesforceBearerToken: '',
                instanceURL: ''
            };
            request.post(
                'https://login.salesforce.com/services/oauth2/token',
                {
                    form: {
                        grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer',
                        assertion: jwtToken
                    }
                },
                (err, response, body) => {
                    if (!err && response.statusCode == 200) {
                        result.salesforceBearerToken = JSON.parse(body)['access_token'];
                        result.instanceURL = JSON.parse(body)['instance_url'];
                        resolve(result);
                    } else {
                        reject('Error obtaining Salesforce Bearer Token.');
                    }
                }
            );
            return result;
        });
    }

    static getDataCloudBearerToken(salesforceBearerToken, instanceURL) {
        if (salesforceBearerToken === '' || instanceURL === '') {
            throw new CategoryRepositoryException('Salesforce Bearer Token and Instance URL is required');
        }
        return new Promise((resolve, reject) => {
            let result = {
                dataCloudBearerToken: '',
                instanceURL: ''
            };
            request.post(
                instanceURL + '/services/a360/token',
                {
                    form: {
                        grant_type: 'urn:salesforce:grant-type:external:cdp',
                        subject_token: salesforceBearerToken,
                        subject_token_type: 'urn:ietf:params:oauth:token-type:access_token'
                    }
                },
                (err, response, body) => {
                    if (!err && response.statusCode == 200) {
                        result.dataCloudBearerToken = JSON.parse(body)['access_token'];
                        result.instanceURL = JSON.parse(body)['instance_url'];
                        resolve(result);
                    } else {
                        reject('Error obtaining Data Cloud Bearer Token.');
                    }
                }
            );
            return result;
        });
    }

    static insertCategory(dataCloudBearerToken, instanceURL, category) {
        if (dataCloudBearerToken === '' || instanceURL === '' || category === '') {
            throw new CategoryRepositoryException('Data Cloud Bearer Token, Instance URL and categories is required');
        }

        return new Promise((resolve, reject) => {
            request.post(
                'https://' + instanceURL + '/api/v1/ingest/sources/new_main_connector/category',
                {
                    json: {
                        data: Array.isArray(category) ? category : [category]
                    },
                    auth: {
                        bearer: dataCloudBearerToken
                    }
                },
                (err, response) => {
                    if (!err && response.statusCode == 202) {
                        resolve(response.body);
                    } else {
                        reject('Error inserting records.');
                    }
                }
            );
        });
    }
}

module.exports = CategoryRepository;
