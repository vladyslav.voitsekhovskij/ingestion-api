/**
 * Created by vladyslavviotsekhovskyi on 08.08.2023.
 */

const ChainHandler = require('./ChainHandler');
const CSVFileReader = require('../FileReaders/CSVFileReader');

class CSVChainHandler extends ChainHandler {
    handle(request) {
        return request.includes('.csv') ? CSVFileReader.readData(request) : super.handle(request);
    }
}

module.exports = CSVChainHandler;
