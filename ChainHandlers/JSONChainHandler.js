/**
 * Created by vladyslavviotsekhovskyi on 08.08.2023.
 */

const ChainHandler = require('./ChainHandler');
const JSONFileReader = require('../FileReaders/JSONFileReader');

class JSONChainHandler extends ChainHandler {
    handle(request) {
        return request.includes('json') ? JSONFileReader.readData(request) : super.handle(request);
    }
}

module.exports = JSONChainHandler;
