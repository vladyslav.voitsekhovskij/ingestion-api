/**
 * Created by vladyslavviotsekhovskyi on 08.08.2023.
 */
const FileReaderException = require('../Exceptions/FileReaderException');

class ChainHandler {
    _nextHandler;
    setNext(handler) {
        this._nextHandler = handler;
        return handler;
    }

    handle(request) {
        if (this._nextHandler) {
            return this._nextHandler.handle(request);
        }
        throw new FileReaderException('Such file formats are not supported');
    }
}

module.exports = ChainHandler;
