/**
 * Created by vladyslavviotsekhovskyi on 03.08.2023.
 */

class CategoryServiceException extends Error {
    constructor(message) {
        super(message);
        this.name = 'CategoryServiceException';
    }
}

module.exports = CategoryServiceException;
