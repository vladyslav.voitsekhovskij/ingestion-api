/**
 * Created by vladyslavviotsekhovskyi on 03.08.2023.
 */

class FileReaderException extends Error {
    constructor(message) {
        super(message);
        this.name = 'FileReaderException';
    }
}

module.exports = FileReaderException;
