/**
 * Created by vladyslavviotsekhovskyi on 03.08.2023.
 */

class CategoryControllerException extends Error {
    constructor(message) {
        super(message);
        this.name = 'CategoryControllerException';
    }
}

module.exports = CategoryControllerException;
