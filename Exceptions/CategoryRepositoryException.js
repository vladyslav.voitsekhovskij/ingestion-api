/**
 * Created by vladyslavviotsekhovskyi on 03.08.2023.
 */

class CategoryRepositoryException extends Error {
    constructor(message) {
        super(message);
        this.name = 'CategoryRepositoryException';
    }
}

module.exports = CategoryRepositoryException;
