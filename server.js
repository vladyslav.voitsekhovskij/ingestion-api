/**
 * Created by vladyslavviotsekhovskyi on 02.08.2023.
 */

const dotenv = require('dotenv');
const express = require('express');
const CategoryController = require('./CategoryController');
const { join } = require('path');
const app = express();
app.use(express.json());

const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 3002;

function authentication(req, res, next) {
    const authHeader = req.headers.authorization;
    console.log(authHeader);

    if (!authHeader) {
        let err = new Error('You are not authenticated!');
        res.setHeader('WWW-Authenticate', 'Basic');
        err.status = 401;
        return next(err);
    }

    const auth = new Buffer.from(authHeader.split(' ')[1], 'base64').toString().split(':');
    const user = auth[0];
    const pass = auth[1];

    if (user === process.env.AUTH_USER && pass === process.env.AUTH_PASS) {
        next();
    } else {
        let err = new Error('You are not authenticated!');
        res.setHeader('WWW-Authenticate', 'Basic');
        err.status = 401;
        return next(err);
    }
}

app.use(authentication);
app.use(express.static(join(__dirname, 'public')));

app.get('/api/v1/endpoint', (req, res) => {
    res.json({ success: true });
});

app.post('/api/insertCategory', (req, res) => {
    CategoryController.insertCategory(req.body.data)
        .then(() => {
            res.send({ message: 'Records were successfully inserted!' });
        })
        .catch(err => {
            res.status(404);
            res.send({ message: err.message });
        });
});

app.listen(PORT, () => console.log(`✅  API Server started: http://${HOST}:${PORT}/api/v1/endpoint`));
