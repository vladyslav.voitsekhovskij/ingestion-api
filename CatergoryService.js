/**
 * Created by vladyslavviotsekhovskyi on 03.08.2023.
 */

const CategoryRepository = require('./CategoryRepository');
const CategoryServiceException = require('./Exceptions/CategoryServiceException');
const TokenCache = require('./CacheControl/TokenCache');
require('dotenv').config();

class CategoryService {
    static async insertCategory(categories) {
        if (categories === '') {
            throw new CategoryServiceException('Category records can not be empty');
        }
        try {
            let cacheData = await TokenCache.getCache();
            let insertResponse = await CategoryRepository.insertCategory(cacheData.dataCloudBearerToken, cacheData.instanceURL, categories);
            console.log(insertResponse);
        } catch (error) {
            console.log(error);
        }
    }
}

module.exports = CategoryService;
