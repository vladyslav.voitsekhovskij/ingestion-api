/**
 * Created by vladyslavviotsekhovskyi on 03.08.2023.
 */

const fs = require('fs');
const { parse, CsvError } = require('csv-parse');

class CSVFileReader {
    static readData(path) {
        return new Promise((resolve, reject) => {
            let data = [];
            fs.createReadStream(path)
                .pipe(
                    parse({
                        delimiter: ',',
                        columns: true,
                        ltrim: true
                    })
                )
                .on('data', row => data.push(row))
                .on('error', error => console.log(error))
                .on('end', () => resolve(data));
        });
    }
}

module.exports = CSVFileReader;
