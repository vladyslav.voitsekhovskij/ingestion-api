/**
 * Created by vladyslavviotsekhovskyi on 03.08.2023.
 */

const JSONChainHandler = require('../ChainHandlers/JSONChainHandler');
const CSVChainHandler = require('../ChainHandlers/CSVChainHandler');

class FileReader {
    static readData(path) {
        const jsonHandler = new JSONChainHandler();
        const csvHandler = new CSVChainHandler();
        jsonHandler.setNext(csvHandler);

        return new Promise((resolve, reject) => {
            resolve(jsonHandler.handle(path));
        });
    }
}

module.exports = FileReader;
