/**
 * Created by vladyslavviotsekhovskyi on 03.08.2023.
 */

const fs = require('fs');
const FileReader = require('./FileReader');

class JSONFileReader {
    static readData(path) {
        return JSON.parse(fs.readFileSync(path));
    }
}

module.exports = JSONFileReader;
