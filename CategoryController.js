/**
 * Created by vladyslavviotsekhovskyi on 03.08.2023.
 */

const path = require('path');
const FileReader = require('./FileReaders/FileReader');
const CategoryService = require('./CatergoryService');
const CategoryControllerException = require('./Exceptions/CategoryControllerException');

//todo: add basic authentification
class CategoryController {
    static async insertCategory(data) {
        if (typeof data === 'object') {
            await CategoryService.insertCategory(data);
            return;
        }
        if (data !== path.basename(data)) {
            await FileReader.readData(data)
                .then(async result => {
                    await CategoryService.insertCategory(result);
                })
                .catch(err => {
                    throw new CategoryControllerException(err);
                });
            return;
        }
        throw new CategoryControllerException('Category records or path can not be empty');
    }
}

module.exports = CategoryController;
