/**
 * Created by vladyslavviotsekhovskyi on 09.08.2023.
 */
const Cache = require('./Cache');
const CategoryRepository = require('../CategoryRepository');
const NodeCache = require('node-cache');
require('dotenv').config();

class TokenCache extends Cache {
    static cache = new NodeCache();
    static async getCache() {
        if (!this.cache.get('data-cloud-token')) {
            let salesforceResponse = await CategoryRepository.getSalesforceBearerToken(process.env.JWT_TOKEN);
            let dataCloudResponse = await CategoryRepository.getDataCloudBearerToken(salesforceResponse.salesforceBearerToken, salesforceResponse.instanceURL);

            this.cache.set('data-cloud-token', dataCloudResponse);
        }
        return this.cache.get('data-cloud-token');
    }
}

module.exports = TokenCache;
